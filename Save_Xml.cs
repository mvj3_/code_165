//保存在工程文件存放的位置;
        private async void saveXml()
        {
            //保存在本地应用程序的根目录下
            StorageFolder sFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync("UserInputData", CreationCollisionOption.OpenIfExists);
            StorageFile sFile = await sFolder.CreateFileAsync("mydata.xml", CreationCollisionOption.OpenIfExists);
            await docElement.SaveToFileAsync(sFile);
        }