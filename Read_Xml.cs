//读取xml文件:
//分为两种,一是从工程的文件夹下读取;二是从工程存放的文件位置读取
//从工程存放的文件位置读取:
 private async void IsolatedStorage()
        {
            var _name = "mydata.xml";
            var _Folder = Windows.Storage.ApplicationData.Current.LocalFolder;
            try
            {
                var _File = await _Folder.GetFileAsync(_name);
                XmlLoadSettings loadsettings = new XmlLoadSettings();
                loadsettings.ProhibitDtd = false;
                loadsettings.ResolveExternals = false;
                XmlDocument xmlDocument = await XmlDocument.LoadFromFileAsync(_File, loadsettings);
                //XmlNodeList nodelist = RootXmlDoc.GetElementsByTagName("curve");
                XmlNodeList curveNodeList = xmlDocument.GetElementsByTagName("curve");
                // reDrawPen(curveNodeList, 0);
                XmlNodeList sealNodeList = xmlDocument.GetElementsByTagName("seal");
            }
            catch
            {

            }
            //reDrawStamp(sealNodeList);
        }
//从项目工程的文件夹下读取：
 private async void readXml()
        {
            string xml = @"Assets\mydata.xml";
            StorageFolder installationFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            StorageFile storageFile = await installationFolder.GetFileAsync(xml);
            XmlLoadSettings loadsettings = new XmlLoadSettings();
            loadsettings.ProhibitDtd = false;
            loadsettings.ResolveExternals = false;
            XmlDocument xmlDocument = await XmlDocument.LoadFromFileAsync(storageFile, loadsettings);
            //XmlNodeList nodelist = RootXmlDoc.GetElementsByTagName("curve");
            XmlNodeList curveNodeList = xmlDocument.GetElementsByTagName("curve");
            reDrawPen(curveNodeList, 0);
            XmlNodeList sealNodeList = xmlDocument.GetElementsByTagName("seal");
            reDrawStamp(sealNodeList);
        }